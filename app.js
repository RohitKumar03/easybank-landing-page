const toggleButton = document.getElementsByClassName('toggle-button')[0]
const navbarLinks = document.getElementsByClassName('navbar-links')[0]

toggleButton.addEventListener('click', () => {
    if (!navbarLinks.classList.contains('active')) {
        navbarLinks.classList.add('active')
        toggleButton.src = './images/icon-close.svg'
    } else {
        navbarLinks.classList.remove('active')
        toggleButton.src = './images/icon-hamburger.svg'
    }

})